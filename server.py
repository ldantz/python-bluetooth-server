from bluetooth import *

server_sock=BluetoothSocket( RFCOMM )
server_sock.bind(("",PORT_ANY))
server_sock.listen(1)

port = server_sock.getsockname()[1]

uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"

advertise_service( server_sock, "SampleServer",
                   service_id = uuid
                   # service_classes = [ uuid, SERIAL_PORT_CLASS ],
                   # profiles = [ SERIAL_PORT_PROFILE ],
                   # protocols = [ RFCOMM_UUID ]
                    )

print ("Waiting for connection on RFCOMM channel %d" % port)

client_sock, client_info = server_sock.accept()
print ("Accepted connection from ", client_info)

try:
    while True:
        data = client_sock.recv(1024)
        if len(data) == 0: break
        data_str = data.decode('utf8')
        if "stop" in data_str:
            course_data = "report#{course_number : '28747',course_duration : '01:58:05',course_co2 : '1760',course_km : '12.2',course_price : '15.51'}"
            course_data = course_data.encode('utf8')
            client_sock.send(course_data)
        print ("received [%s]" % data)
except IOError:
    pass

print ("disconnected")

client_sock.close()
server_sock.close()
print ("all done")
